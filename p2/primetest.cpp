/*
 * CSc103 Project 2: prime numbers.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cmath>

int main()
{
   bool prime = true;
   unsigned long n;
   while (cin >> n)
   {
     for (unsigned long i = 2; i <= sqrt(n); i++)
     {
       if (n % i == 0)
        {
          prime = false;
         cout << "0" << endl;
         }
       else (prime = true);
        {
          cout << "1" << endl;
        }
     break;
         }
   }


  return 0;
}

