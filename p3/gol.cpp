/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <iostream>
using std :: cout;
using std :: cin;
using std :: endl;
#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

char text[3] = ".O";

int main(int argc, char *argv[])
{
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1)
		{
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
}


bool size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g) //count neighbors of cell i,j on grid g
{
		int nbrCount = 0;
		if (g[i][j] = false)
		{
		// upper neighbor
			if ( g[(i-1+length) % length] [j] == true)
			{
				nbrCount++;
			}
		// lower neighbor
			if ( g[(i+1) % length] [j] == true)
			{
				nbrCount++;
			}

		// left neighbor
			if ( g[i] [(j-1+width) % width] == true)
			{
				nbrCount++;
			}
		// right neighbor
			if ( g[i] [(j+1) % width] == true)
			{
				nbrCount++;
			}
		// upper left
			if ( g[(i-1+length) % length] [(j-1+width) % width] == true)
			{
				nbrCount++;
			}
		// lower left
			if ( g[(i+1) % length] [(j-1+width) % width] == true)
			{
				nbrCount++;
			}
		// upper right
			if ( g[(i-1-length) % length] [(j+1) % width] == true)
			{
				nbrCount++;
			}
		// lower right
			if ( g[(i+1) % length] [(j-1+width) % width] == true)
			{
				nbrCount++;
			}
return nbrCount;
	}
}
void update(int nbrCount& )
{
		if ( nbrCount < 2 || nbrCount > 3)
		 {
				nbrCount = '.';
		 }
		if ( nbrCount == 3 )
		 {
				nbrCount = 'O';
		 }
}

int initFromFile(const string& fname); //read initial state from file
{
  size_t i,j;
  FILE* f = fopen("/tmp/gol-world-current","rb");
  if (f == NULL)
  {
    cout << "error!" << endl;
    exit(1);
  }

  char c;
  fread(&c,1,1,f);

  vector <vector<bool> > vec2;
  vector<bool> vec1;
  char c = '.';
  vec2[i][j] = false;
  while(cin >> c)
  {
    while (fread(&c, 1, 1, f) != 0)
    {
      if (c == '\n')
      {
        vec2.push_back(vec1);
        vec1.clear();
      }
      else
      {
        if (c == '.')
        vec1.push_back(false);
        else
				vec1.push_back(true);
      }
    }
  }
fclose(f);
}


void dumpState (FILE* f, vector <vector<bool> > vec2, vector<bool> vec1) //write the state to a file
{

	FILE* f = fopen ("/tmp/gol-world-current", "wb");

	char c;
	bool state;
	for (int j = 0; j< vec2.size(); j++)
		{
			for(int i = 0, i < vec1.size(); i++)
			{
				state = nbrCount(i, j, vec2);
				switch(state)
				{
						case true:
	   					 c = 'O';
						case false:
	   					 c = '.';
				}

			fwrite(&c, 1, 1, f); //write a byte to f
			/* close f when done */
			}
		fclose(f);
		}


	mainLoop();
	return 0;
}

void mainLoop()
{
	/* update, write, sleep */
	update();
	dumpState(FILE* f);
	sleep (10);
}

